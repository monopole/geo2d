#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
#[derive(Clone, Copy, PartialEq, Debug)]
/// A point in 2D space.
pub struct Point {
    /// x-coordinate of the point
    pub x: f64,

    /// y-coordinate of the point
    pub y: f64,
}

impl Point {
    /// The origin
    pub const ZERO: Vec2d = Vec2d::new(0.0, 0.0);

    /// Create a new point from coordinates
    pub fn new(x: f64, y: f64) -> Point {
        Point { x, y }
    }
}

impl std::ops::Add<Vec2d> for Point {
    type Output = Point;

    fn add(self, other: Vec2d) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::Sub for Point {
    type Output = Vec2d;

    fn sub(self, other: Point) -> Vec2d {
        Vec2d::new(self.x - other.x, self.y - other.y)
    }
}

impl std::ops::AddAssign<Vec2d> for Point {
    fn add_assign(&mut self, other: Vec2d) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl From<(f64, f64)> for Point {
    fn from(src: (f64, f64)) -> Point {
        Point::new(src.0, src.1)
    }
}

impl From<[f64; 2]> for Point {
    fn from(src: [f64; 2]) -> Point {
        Point::new(src[0], src[1])
    }
}

impl From<Point> for [f64; 2] {
    fn from(src: Point) -> [f64; 2] {
        [src.x, src.y]
    }
}

impl From<Point> for (f64, f64) {
    fn from(src: Point) -> (f64, f64) {
        (src.x, src.y)
    }
}

#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
#[derive(Clone, Copy, Debug, PartialEq)]
/// A vector in 2D space, represented by components
pub struct Vec2d {
    /// x-component of the vector
    pub x: f64,

    /// y-component of the vector
    pub y: f64,
}

impl Vec2d {
    /// The zero vector (<0, 0>)
    pub const ZERO: Vec2d = Vec2d::new(0.0, 0.0);

    /// Create a vector by components
    pub const fn new(x: f64, y: f64) -> Vec2d {
        Vec2d { x, y }
    }

    /// Create a unit-length vector at a specific angle (in radians)
    pub fn for_angle(angle: f64) -> Vec2d {
        Vec2d::new(angle.cos(), angle.sin())
    }

    /// Get the angle of the vector in radians
    pub fn angle(self) -> f64 {
        self.y.atan2(self.x)
    }

    /// Get the magnitude of the vector.
    pub fn magnitude(self) -> f64 {
        (self.y.powi(2) + self.x.powi(2)).sqrt()
    }
}

impl std::ops::Mul<f64> for Vec2d {
    type Output = Vec2d;

    fn mul(self, other: f64) -> Vec2d {
        Vec2d::new(self.x * other, self.y * other)
    }
}

impl std::ops::Div<f64> for Vec2d {
    type Output = Vec2d;

    fn div(self, other: f64) -> Vec2d {
        Vec2d::new(self.x / other, self.y / other)
    }
}

impl std::ops::AddAssign for Vec2d {
    fn add_assign(&mut self, other: Vec2d) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl std::ops::SubAssign for Vec2d {
    fn sub_assign(&mut self, other: Vec2d) {
        self.x -= other.x;
        self.y -= other.y;
    }
}

impl From<(f64, f64)> for Vec2d {
    fn from(src: (f64, f64)) -> Vec2d {
        Vec2d::new(src.0, src.1)
    }
}

impl From<[f64; 2]> for Vec2d {
    fn from(src: [f64; 2]) -> Vec2d {
        Vec2d::new(src[0], src[1])
    }
}

impl From<Vec2d> for [f64; 2] {
    fn from(src: Vec2d) -> [f64; 2] {
        [src.x, src.y]
    }
}

impl From<Vec2d> for (f64, f64) {
    fn from(src: Vec2d) -> (f64, f64) {
        (src.x, src.y)
    }
}
